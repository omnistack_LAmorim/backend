const Post = require('../models/Post');

module.exports = {
    async store(req, res) {
        const post = await Post.findById(req.params.id);

        post.likes += 1;

        await post.save();

        //Todos os usuários que estão conectados irão receber uma mensagem através do socket.io
        //Em tempo real 
        req.io.emit('like', post);

        return res.json(post);
    }
}
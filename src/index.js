const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const cors = require('cors');

const app = express();

const server = require('http').Server(app);
//Permite receber ou enviar requisições para todos os usuários que estão conectados na nosaa aplicação
const io = require('socket.io')(server);

mongoose.connect('mongodb+srv://semana:semana@testdeveloper-immts.mongodb.net/test?retryWrites=true&w=majority', {
    useNewUrlParser: true,
})

app.use( (req, res, next) => {
    req.io = io;

    //Garante que o restante do código seja executado
    next();
})

app.use(cors());

app.use('/files', express.static(path.resolve(__dirname, '..', 'uploads', 'resized')));

app.use(require('./routes'));

server.listen(3333);

